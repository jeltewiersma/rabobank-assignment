import { Component, OnInit } from '@angular/core';
import { FileInputHandlerService } from '../services/file-input-handler.service';
@Component({
  selector: 'app-visualize-from-file',
  templateUrl: './visualize-from-file.component.html',
  styleUrls: ['./visualize-from-file.component.sass'],
  providers: [FileInputHandlerService]
})
export class VisualizeFromFileComponent implements OnInit {

  constructor(private fileInputHandlerService: FileInputHandlerService) { }
  users = [];

  ngOnInit() {
  }

  handleFileInput(files) {
    if (files.length > 0) {
      this.fileInputHandlerService.convertFile(files[0], (event) => {
        const data = event.target.result.split(/\n/);
        const categories = data[0].split(',');
        for (let i = 1; i < data.length; i++) {
          this.users.push(this.createUser(categories, data[i].split(',')));
        }
      });
    } else {
      console.error('No files found');
    }
  }

  createUser(categories, values): object {
    const user = {};
    for (const category in categories) {
      if (category && categories.length === values.length) {
        const categoryName = categories[category];
        user[categoryName] = values[category];
      }
    }
    return user;
  }

  sortByIssueCount() {
    this.users.sort((a, b) => {
      const keyA = a['"Issue count"'];
      const keyB = b['"Issue count"'];
      if (keyA < keyB) {
        return -1;
      }
      if (keyA > keyB) {
        return 1;
      }
      return 0;
  });
  }
}
