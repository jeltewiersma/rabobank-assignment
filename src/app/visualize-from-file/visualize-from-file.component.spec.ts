import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizeFromFileComponent } from './visualize-from-file.component';

describe('VisualizeFromFileComponent', () => {
  let component: VisualizeFromFileComponent;
  let fixture: ComponentFixture<VisualizeFromFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizeFromFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizeFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render input tag of type file', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#upload-file')).toBeTruthy();
  });
});
