import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { VisualizeFromFileComponent } from './visualize-from-file/visualize-from-file.component';

@NgModule({
  declarations: [
    AppComponent,
    VisualizeFromFileComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
