import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class FileInputHandlerService {
  constructor() { }

  convertFile(file, onLoadCallback): any {
    const reader = new FileReader();
    reader.onload = onLoadCallback;
    reader.readAsText(file, 'UTF8');
    reader.onerror = (event) => {
        console.log('Reading went wrong ', event);
    };
  }
}
