import { TestBed } from '@angular/core/testing';

import { FileInputHandlerService } from './file-input-handler.service';

describe('FileInputHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FileInputHandlerService = TestBed.get(FileInputHandlerService);
    expect(service).toBeTruthy();
  });
});
